package main;

/**
 * @author Sean Lindell
 */
public class StateTicTacToe {
	
	/**
	 * Stores where Xs and Os have been placed.
	 * 0s represent blank tiles
	 * 1s represent Xs
	 * 2s represent Os
	 */
	private int[] gameBoard = new int[9];
	
	/**
	 * Constructor method, makes board blank
	 */
	public StateTicTacToe() {
		setBoardBlank();
	}
	
	/**
	 * Constructor method, makes new board by copying an old board
	 */
	public StateTicTacToe(StateTicTacToe oldBoard) {
		for (int i = 0; i < gameBoard.length; i++) {
			this.gameBoard[i] = oldBoard.gameBoard[i];
		}
	}
	
	/**
	 * Sets the board to blank
	 */
	void setBoardBlank() {
		for (int i = 0; i < gameBoard.length; i++) {
			gameBoard[i] = 0;
		}
	}
	
	/**
	 * Checks the board to see if either player has won.
	 * @return returns 0 if no one has won yet, 1 if player 1 won, 2 if player 2 won, 3 if the game is in a tied state
	 */
	int checkIfPlayerHasWon() {
		// Check rows
		for (int i = 0; i < 3; i++) {
			if (gameBoard[3*i]==gameBoard[(3*i)+1] && gameBoard[3*i+2]==gameBoard[(3*i)+1] && gameBoard[3*i] != 0) {
				return gameBoard[3*i];
			}
		}
		
		// Check columns
		for (int i = 0; i < 3; i++) {
			if (gameBoard[i]==gameBoard[3+i] && gameBoard[3+i]==gameBoard[6+i] && gameBoard[i] != 0) {
				return gameBoard[i];
			}
		}
		
		// Check crosses
		if (gameBoard[4] != 0) {
			if (gameBoard[0] == gameBoard[4] && gameBoard[8] == gameBoard[4]) {
				return gameBoard[4];
			}
			if (gameBoard[2] == gameBoard[4] && gameBoard[6] == gameBoard[4]) {
				return gameBoard[4];
			}
		}
		
		// Check if board is full for a tie
		boolean full = true;
		for (int i = 0; i < gameBoard.length; i++) {
			if(gameBoard[i]==0) {
				full = false;
			} 
		}
		if(full) {
			return 3;
		}
		
		// No one has won, and the board is not full
		return 0;
	}
	
	/**
	 * Set the cell at the given index to a given value. Can only replace blank tiles
	 * @param index the cell to change the value of
	 * @param playerID what to change the cell to. Should be 1 for player 1 and 2 for player 2
	 * @return true if the replacement was successful, false if it was not.
	 */
	boolean setValue(int index, int playerID) {
		if (index < 9) {
			if (gameBoard[index] == 0) {
				gameBoard[index] = playerID;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the value of the given cell.
	 * @param index cell to get the value of. Must be from 0 to 8
	 * @return the value of the cell at the given index. Will be 0 for empty, 1 for an X, and 2 for an O.
	 */
	int getValue(int index) {
		return gameBoard[index];
	}
}
