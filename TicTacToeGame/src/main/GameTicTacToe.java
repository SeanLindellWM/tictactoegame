package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * @author Sean Lindell
 */
public class GameTicTacToe extends JPanel{

	private static final long serialVersionUID = 1L;
	private Color BG_COLOR = Color.lightGray;
	private String currentTextString = "Player 1's Turn";
	private int playerTurn = 1;
	private String currentAppState = "Title";
	private StateTicTacToe gamestate = new StateTicTacToe();
	private boolean currentPlayerIsHuman = false;
	private Random r = new Random();
	private String playerOneState = "Human";
	private String playerTwoState = "Human";
	private int turnNumber = 1;
	
	/**
	 * gametype:
	 * 5 = hard ai vs random ai
	 * 4 = hard ai vs hard ai
	 * 3 = random ai vs random ai
	 * 2 = player vs hard ai
	 * 1 = player vs random ai
	 * 0 = player vs player
	 */
	private int gameType = 0;
	
	/**
	 * This is the constructor method for the TicTacToe game. This handles mouse tracking, and the results of mouse clicks.
	 */
	public GameTicTacToe() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (currentAppState == "Game") {
					if(playerTurn == 0) {
						resetGame();
					}
					else {
						int mx = e.getX();
						int my = e.getY();
						int whereIsMouse = getSquareIndex(mx, my);
						if (whereIsMouse!=33) {
							if(currentPlayerIsHuman) {
								if(gamestate.setValue(whereIsMouse, playerTurn)) {
									switchPlayerTurn();
									turnNumber++;
								}
							}
							else {
								aiMoves();
								switchPlayerTurn();
								turnNumber++;
							}
							checkIfPlayerHasWon();
						}
						else {
							currentAppState = "Title";
						}
						repaint();
					}
				}
				else {
					currentAppState = "Game";
					gamestate.setBoardBlank();
					switch (gameType) {
					case 1:
						playerOneState = "Human";
						playerTwoState = "Random AI";
						currentPlayerIsHuman = true;
						break;
					case 2:
						playerOneState = "Human";
						playerTwoState = "Hard AI";
						currentPlayerIsHuman = true;
						break;
					case 3:
						playerOneState = "Random AI";
						playerTwoState = "Random AI";
						currentPlayerIsHuman = false;
						break;
					case 4:
						playerOneState = "Hard AI";
						playerTwoState = "Hard AI";
						currentPlayerIsHuman = false;
						break;
					case 5:
						playerOneState = "Hard AI";
						playerTwoState = "Random AI";
						currentPlayerIsHuman = false;
						break;
					default:
						playerOneState = "Human";
						playerTwoState = "Human";
						currentPlayerIsHuman = true;
						break;
					}
					randomizeStartingPlayer();
					repaint();
				}
			}
		});
		addMouseWheelListener(new MouseAdapter() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (currentAppState == "Title") {
					if(e.getWheelRotation()>0) {
						gameType++;
					}
					else {
						gameType--;
					}
					if (gameType == 6) {
						gameType = 0;
					}
					if (gameType == -1) {
						gameType = 5;
					}
					repaint();
				}
			}
		});
	}
	
	private void aiMoves() {
		if((playerTurn==1 && playerOneState=="Hard AI") || (playerTurn==2 && playerTwoState=="Hard AI")) {
			hardAIMoves();
		}
		else {
			randomAIMoves();
		}
	}
	
	private void randomAIMoves() {
		while(!gamestate.setValue(r.nextInt(9), playerTurn)) {
			System.out.println("AI " + playerTurn + ": I couldn't place where I wanted to!");
		}
	}
	
	private void hardAIMoves() {
		if(winThisTurn()) {
			System.out.println("I win!");
		}
		else if(preventOpponentWin()) {
			System.out.println("I almost lost!");
		}
		else if(turnNumber==1) {
			gamestate.setValue(0, playerTurn);
		}
		else if (turnNumber==2) {
			if (gamestate.getValue(4)!=0) {
				gamestate.setValue(0, playerTurn);
			}
			else {
				gamestate.setValue(4, playerTurn);
			}
		}
		else if(turnNumber==3) {
			if(gamestate.getValue(8)!=0) {
				gamestate.setValue(2, playerTurn);
			}
			else if (gamestate.getValue(2)!=0||gamestate.getValue(6)!=0) {
				gamestate.setValue(8, playerTurn);
			}
			else if (gamestate.getValue(4)==0) {
				gamestate.setValue(4, playerTurn);
			}
			else {
				gamestate.setValue(8, playerTurn);
			}
		}
		else if (turnNumber==4) {
			if(gamestate.getValue(4)==playerTurn) {
				int numberOfLettersOnPlus = 4;
				for (int i = 1; i < 8; i+=2) {
					if (gamestate.getValue(i)==0) {
						numberOfLettersOnPlus--;
					}
				}
				if(numberOfLettersOnPlus==0) {
					gamestate.setValue(1, playerTurn);
				}
				else if(numberOfLettersOnPlus==2) {
					gamestate.setValue(0, playerTurn);
				}
				else {
					if (gamestate.getValue(0)==oppositeTurn(playerTurn)) {
						gamestate.setValue(8, playerTurn);
					}
					else if (gamestate.getValue(8)==oppositeTurn(playerTurn)) {
						gamestate.setValue(0, playerTurn);
					}
					else if (gamestate.getValue(2)==oppositeTurn(playerTurn)) {
						gamestate.setValue(6, playerTurn);
					}
					else {
						gamestate.setValue(2, playerTurn);
					}
				}
			}
			else {
				if(!gamestate.setValue(6, playerTurn)) {
					gamestate.setValue(2, playerTurn);
				}
			}
		}
		else {
			randomAIMoves();
		}
	}
	
	private boolean winThisTurn() {
		for (int i = 0; i < 9; i++) {
			StateTicTacToe testBoard = new StateTicTacToe(gamestate);
			if(testBoard.setValue(i, playerTurn)) {
				if(testBoard.checkIfPlayerHasWon()==playerTurn) {
					gamestate.setValue(i, playerTurn);
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean preventOpponentWin() {
		for (int i = 0; i < 9; i++) {
			StateTicTacToe testBoard = new StateTicTacToe(gamestate);
			if(testBoard.setValue(i, oppositeTurn(playerTurn))) {
				if(testBoard.checkIfPlayerHasWon()==oppositeTurn(playerTurn)) {
					gamestate.setValue(i, playerTurn);
					return true;
				}
			}
		}
		return false;
	}

	private void switchPlayerTurn() {
		if (playerTurn == 1) {
			playerTurn = 2;
			currentTextString = "Player 2's Turn";
			if(playerTwoState=="Human") {
				currentPlayerIsHuman = true;
			}
			else {
				currentPlayerIsHuman = false;
				currentTextString = currentTextString + " (AI)";
			}
		}
		else if (playerTurn == 2) {
			playerTurn = 1;
			currentTextString = "Player 1's Turn";
			if(playerOneState=="Human") {
				currentPlayerIsHuman = true;
			}
			else {
				currentPlayerIsHuman = false;
				currentTextString = currentTextString + " (AI)";
			}
		}
	}
	
	private int oppositeTurn(int inputTurn) {
		if(inputTurn==1) {
			return 2;
		}
		return 1;
	}
	
	private void checkIfPlayerHasWon() {
		int currentWinState = gamestate.checkIfPlayerHasWon();
		if(currentWinState != 0) {
			playerTurn = 0;
			if (currentWinState == 1) {
				currentTextString = "Player 1 Wins";
			}
			else if (currentWinState == 2) {
				currentTextString = "Player 2 Wins";
			}
			else {
				currentTextString = "Tie";
			}
		}
	}
	
	/**
	 * This method handles the drawing of the board whenever it changes.
	 * @param g The graphics object to be drawn onto
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		switch (currentAppState) {
		case "Title":
			drawTitle(g);
			break;
		case "Game":
			drawGame(g);
			break;
		default:
			drawTitle(g);
			break;
		}
	}
	
	private void drawTitle(Graphics g) {
		clearCanvas(g);
		drawTitleText(g);
		drawModeText(g);
	}

	private void drawGame(Graphics g) {
		clearCanvas(g);
		drawTicTacToeLines(g);
		drawGameState(g);
		drawGameText(g);
	}
	
	private void drawTitleText(Graphics g) {
		g.setColor(Color.black);
		g.setFont(new Font("Arial", Font.BOLD,90));
		g.drawString("Tic Tac Toe", 50, 100);
		g.setFont(new Font("Arial", Font.BOLD,30));
		g.drawString("By: Sean Lindell", 180, 150);
		g.drawString("Click to Begin", 200, 600);
		g.drawString("Use Scroll Wheel to Change Mode", 60, 650);
	}
	
	private void clearCanvas(Graphics g) {
		g.setColor(BG_COLOR);
		g.fillRect(0, 0, this.getSize().width, this.getSize().height);
	}
	
	private void drawModeText(Graphics g) {
		switch (gameType) {
		case 0:
			g.setColor(Color.magenta);
			g.fillRect(0, 270, this.getSize().width, 100);
			g.setColor(Color.black);
			g.drawString("Player VS Player", 180, 325);
			break;
		case 1:
			g.setColor(Color.blue);
			g.fillRect(0, 270, this.getSize().width, 100);
			g.setColor(Color.black);
			g.drawString("Player VS Random AI", 180, 325);
			break;
		case 2:
			g.setColor(Color.green);
			g.fillRect(0, 270, this.getSize().width, 100);
			g.setColor(Color.black);
			g.drawString("Player VS Hard AI", 180, 325);
			break;
		case 3:
			g.setColor(Color.yellow);
			g.fillRect(0, 270, this.getSize().width, 100);
			g.setColor(Color.black);
			g.drawString("Random AI VS Random AI", 112, 325);
			break;
		case 4:
			g.setColor(Color.orange);
			g.fillRect(0, 270, this.getSize().width, 100);
			g.setColor(Color.black);
			g.drawString("Hard AI VS Hard AI", 164, 325);
			break;
		default:
			g.setColor(Color.red);
			g.fillRect(0, 270, this.getSize().width, 100);
			g.setColor(Color.black);
			g.drawString("Hard AI VS Random AI", 164, 325);
			break;
		}

	}
	
	private void drawTicTacToeLines(Graphics g) {
		g.setColor(Color.black);
		g.drawLine(200, 0, 200, 600);
		g.drawLine(400, 0, 400, 600);
		g.drawLine(0, 200, 600, 200);
		g.drawLine(0, 400, 600, 400);
		g.drawLine(0, 600, 600, 600);
	}
	
	/**
	 * This method draws the Xs and Os of the game board
	 * @param g The graphics object to draw on
	 */
	private void drawGameState(Graphics g) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				int currentSpotVal = gamestate.getValue(i+(j*3));
				g.setFont(new Font("Arial", Font.BOLD,90));
				String text = "";
				if (currentSpotVal == 1) {
					g.setColor(Color.red);
					text = "X";
				}
				else if (currentSpotVal == 2) {
					g.setColor(Color.blue);
					text = "O";
				}
				if (currentSpotVal!=0) {
					g.drawString(text, 70+i*200, 120+j*200);
				}
			}			
		}
	}
	
	/**
	 * This method draws the text at the bottom of the screen
	 * @param g The graphics object to draw on
	 */
	private void drawGameText(Graphics g) {
		if (playerTurn == 1) {
			g.setColor(Color.red);
		}
		else if (playerTurn == 2) {
			g.setColor(Color.blue);
		}
		else {
			g.setColor(Color.black);
		}
		g.setFont(new Font("Arial", Font.BOLD,48));
		g.drawString(currentTextString, 10, 666);
		g.setFont(new Font("Arial", Font.BOLD,12));
		g.setColor(Color.black);
		g.drawString("Back to Title", 520, 680);
	}
	
	private void randomizeStartingPlayer() {
		int loopCount = r.nextInt(2)+1;
		for (int i = 0; i < loopCount; i++) {
			switchPlayerTurn();
		}
	}
	
	/**
	 * This method resets the board and sets up variable for a new game
	 */
	private void resetGame() {
		playerTurn = 1;
		turnNumber = 1;
		if (playerOneState=="Human") {
			currentPlayerIsHuman = true;
		}
		gamestate.setBoardBlank();
		currentTextString = "Player 1's Turn";
		randomizeStartingPlayer();
		repaint();
	}
	
	/**
	 * This method takes the coordinates of the mouse and determines which cell in the grid it corresponds to. The grid is:
	 * 
	 *  ___________
	 * | 0 | 1 | 2 |
	 * |-----------|
	 * | 3 | 4 | 5 |
	 * |-----------|
	 * | 6 | 7 | 8 |
	 * |-----------|
	 * |_________13|
	 * 
	 * @param x x coordinate of the mouse 0 is the farthest left part of the screen, and 615 is the farthest right part.
	 * @param y y coordinate of the mouse 0 is the top of the screen, and 725 is the bottom.
	 * @return the index of the cell the mouse is over (from 0 to 8). If the mouse is not over a cell, it will return a number from 30 to 32, except for how it will return 
	 * 33 if mouse is over the back button.
	 */
	private int getSquareIndex(int x, int y) {
		int xRow = 10;
		int yRow = 10;
		if (y<200) {
			yRow = 0;
		}
		else if (y<400) {
			yRow = 1;
		}
		else if (y<600) {
			yRow = 2;
		}
		if (x<200) {
			xRow = 0;
		}
		else if (x<400) {
			xRow = 1;
		}
		else if (x<600) {
			xRow = 2;
		}
		if (x>515 && y>660) {
			xRow = 3;
		}
		int finalIndex = xRow + (yRow*3);
		return finalIndex;
	}

	/**
	 * Main method to start the game
	 * @param args unused, ignored
	 */
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Tic Tac Toe");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(615,725);
		frame.setResizable(false);
		frame.add(new GameTicTacToe());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
